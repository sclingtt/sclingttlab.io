---
layout: post
title:  "Book Review, Excerpts of 'Warped Passages - Unraveling The Mysteries of The Universe's Hidden Dimensions'"
author: kai
categories: [ reading ]
image: assets/images/multidimension_universe.jpg
tags: [featured]
---

> James Clerk Maxwell, for example, who developed the classical theory of electricity and magnetism, didn't believe in the existence of fundamental units of cahrge such as electrons. George Stoney, who at the end of the nineteenth century proposed the electron as a fundamental unit of charge, didn't believe that scientists would ever isolate electrons from the atoms of which they are components... Dmitri Mendeleev, creator of the periodic table, resisted the notion of valence, which his table encoded. Max Planck, who proposed that the energy carried by light was discontinuous, didn't believe in the reality of the light quanta that were implicit in his own idea. Albert Einstein, who suggested these quanta of light, didn't know that their mechanical properties would permit them to be identified as particles - the photons we now know them to be.

![picasso's protrait of dora maar]({{ site.baseurl }}/assets/images/picasso_dora_maar.jpg "Picasso's Portrait of Dora Maar")

> A cubist painting (for example Picasso's _Portrait of Dora Maar_') presents several projections simultaneously, each from a different angle, and thereby conveys the subject's three-dimensionality.

> Physicists accept this practice and have given it a name - _effective theory_ ... concentrates on the particles and forces that have "effects" at the distance in question.

(draft)
